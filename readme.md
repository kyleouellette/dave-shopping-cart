# Project Task - Shopping Cart

- Create a series of pages with products on them
- Allow users to add products to your shopping cart
- Allow users to select the number of the desired item they would like
- Allow users to see a page that shows all their items, a total, and a total with a 7% sales tax