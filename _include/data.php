<?php

/**
 * File's only purpose is to provide data
 * Feel free to add more items. I've set them up as classes so that it's easy and each item has functionality
 * "Wool socks" has comments to show what's what.
 *
 * $items[] = new Item($name, $price, $description, $onHandCount);
 *
 * To use a class method, you can use arrow notation
 * $item->is_in_stock()
 */
$items = array();

$items[] = new Item(
   'Wool Socks', // title
   14.99,  // price
   'Probably the warmest socks you will ever come across, these puppies will be awesome for New England winters!',  // description
   8 // how many in stock
);

$items[] = new Item(
   'Smoking Trails Boots - one size fits all', 
   98.99, 
   'Looking to be a trail blazer? Oh hell yea bro, me too! So get yourself some of these boots and you\'ll be all set', 
   2
);

$items[] = new Item(
   'Hand warmers', 
   9.99, 
   'Keep dem hands warm! These suckers last up to 12 hours in your mittens', 
   0
);