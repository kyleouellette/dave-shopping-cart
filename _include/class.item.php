<?php

class Item{

   function __construct($name, $price, $description, $in_stock_count){
      $this->name = $name;
      $this->price = $price;
      $this->description = $description;
      $this->in_stock_count = $in_stock_count;
      return $this;
   }

   function is_in_stock(){
      if( $this->in_stock_count > 0 ){
         return true;
      }
      return false;
   }
   
}