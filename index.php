<?php

require('_include/config.php');

?>
<!DOCTYPE html>
<html>
<head>
   <title>The Shop // Home</title>
   <link rel="stylesheet" type="text/css" href="_css/main.css">
</head>
<body>

<?php 

debug($items); 

/**
 * example of checking if an item is in stock
 */
$item = $items[2];

if( $item->is_in_stock() ){
   echo "$item->name is in stock";
}else{
   echo "$item->name is NOT in stock";
}
   
?>


<script type="text/javascript" src="//code.jquery.com/jquery.min.js"></script>
<script type="text/javascript" src="_js/main.js"></script>
</body>
</html>